<?php
require_once(__DIR__ .'/../vendor/autoload.php');

use MatthiasMullie\Minify;

$files = [
	"/vendor/bootstrap/bootstrap.min.css",
	"/vendor/icon-line/css/simple-line-icons.css",
	"/vendor/icon-line-pro/style.css",
    "/vendor/icon-awesome/css/font-awesome.min.css",
    "/vendor/icon-hs/style.css",
    "/vendor/hamburgers/hamburgers.min.css",
    "/vendor/chosen/chosen.css",
    "/vendor/wait-animate.min.css",
    "/vendor/animate.css",
    "/vendor/slick-carousel/slick/slick.css",
	"/vendor/fancybox/jquery.fancybox.css",
	"/css/styles.op-gym.css",
	"/css/custom.css"

];

$file_one = array_shift($files);

$minifier = new Minify\CSS(__DIR__ .  $file_one);

foreach($files as $file){
	$minifier->add(__DIR__ .  $file);
}

// we can even add another file, they'll then be
// joined in 1 output file
//$sourcePath2 = '/path/to/second/source/css/file.css';
//$minifier->add($sourcePath2);

// or we can just add plain CSS
//$css = 'body { color: #000000; }';
//$minifier->add($css);

// save minified file to disk
$minifiedPath = __DIR__ . '/css/site.css';
$minifier->minify($minifiedPath);
