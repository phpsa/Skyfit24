<?php

require_once(__DIR__ .'/../vendor/autoload.php');
use MatthiasMullie\Minify;


$files = [
	"/vendor/jquery/jquery.min.js",
    "/vendor/jquery-migrate/jquery-migrate.min.js",
    "/vendor/popper.min.js",
	"/vendor/bootstrap/bootstrap.min.js",
	"/vendor/chosen/chosen.jquery.js",
    "/vendor/image-select/src/ImageSelect.jquery.js",
    "/vendor/appear.js",
    "/vendor/slick-carousel/slick/slick.js",
    "/vendor/fancybox/jquery.fancybox.js",
	"/vendor/gmaps/gmaps.min.js",
	"/js/hs.core.js",
    "/js/components/hs.header.js",
    "/js/helpers/hs.hamburgers.js",
    "/js/components/hs.scroll-nav.js",
    "/js/components/hs.select.js",
    "/js/components/hs.counter.js",
    "/js/components/hs.carousel.js",
    "/js/components/hs.popup.js",
    "/js/components/gmap/hs.map.js",
    "/js/components/hs.go-to.js",

];

$file_one = array_shift($files);

$minifier = new Minify\JS(__DIR__ .  $file_one);

foreach($files as $file){
	$minifier->add(__DIR__ .  $file);
}


$minifiedPath = __DIR__ . '/js/site.min.js';
$minifier->minify($minifiedPath);